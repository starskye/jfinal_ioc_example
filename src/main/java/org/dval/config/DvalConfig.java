package org.dval.config;

import com.alibaba.druid.filter.logging.Slf4jLogFilter;
import com.alibaba.druid.filter.stat.StatFilter;
import com.alibaba.druid.wall.WallFilter;
import com.jfinal.config.*;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;
import org.dval.controllerfactory.IocControllerFactory;
import org.dval.model._MappingKit;
import org.dval.plugin.IocPlugin;

/**
 * @author duai
 * @version V1.0
 * @Title: jfianlexample
 * @Package org.dval.config
 * @Description: TODO
 * @date 2017-12-11 15:46
 */
public class DvalConfig extends JFinalConfig {
    private Prop systemProp = PropKit.use("systemProp.properties");
    Routes routes;
    @Override
    public void configConstant(Constants constants) {
        constants.setControllerFactory(new IocControllerFactory());
    }

    @Override
    public void configRoute(Routes routes) {
        routes.setBaseViewPath("/WEB-INF/jsp/");
        routes.add(this.routes);
    }

    @Override
    public void configEngine(Engine engine) {
    }

    @Override
    public void configPlugin(Plugins plugins) {
        DruidPlugin druidPlugin = new DruidPlugin(systemProp.get("jdbc.mysql.uri"),
                systemProp.get("jdbc.mysql.user"), systemProp.get("jdbc.mysql.password"));
        druidPlugin.setValidationQuery(systemProp.get("jdbc.mysql.validation_query"));
        druidPlugin.addFilter(new StatFilter());
        WallFilter wall = new WallFilter();
        wall.setDbType("mysql");
        druidPlugin.addFilter(wall);
        druidPlugin.addFilter(new Slf4jLogFilter());
        druidPlugin.setValidationQuery("select 1");
        plugins.add(druidPlugin);
        ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
        arp.setShowSql(true);
        arp.setContainerFactory(new CaseInsensitiveContainerFactory(true));
        arp.setDialect(new MysqlDialect());
        _MappingKit.mapping(arp);
        plugins.add(arp);
        String[] pkgs = systemProp.get("system.annotation.scan").split(",");
        IocPlugin ioc = new IocPlugin(pkgs);
        routes = ioc.getRoutes();
        plugins.add(ioc);
    }

    @Override
    public void configInterceptor(Interceptors interceptors) {

    }

    @Override
    public void configHandler(Handlers handlers) {

    }
}
