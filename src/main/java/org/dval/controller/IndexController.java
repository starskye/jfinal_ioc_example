package org.dval.controller;

import com.jfinal.aop.Before;
import com.jfinal.upload.UploadFile;
import org.dval.Test;
import org.dval.annotation.ControllerKey;
import org.dval.core.ExportExcelTemplateKit;
import org.dval.core.ImportExcelModelKit;
import org.dval.model.Category;

import java.io.IOException;
import java.util.List;

/**
 * @author duai
 * @version V1.0
 * @Title: jfianlexample
 * @Package org.dval
 * @Description: TODO
 * @date 2017-12-11 15:55
 */
@ControllerKey(value = "index",viewPath = "index")
public class IndexController extends BaseController{
    @Before(Test.class)
    public void index() throws IOException {
        List<Category> list = categoryService.findAll();
        ExportExcelTemplateKit.renderExcelTempl(getResponse(),list,"cates","cates.xlsx","类别导出.xlsx");
        renderNull();
    }
    public void upload(){
       UploadFile uploadFile =  getFile("file");
       List<Category> list = ImportExcelModelKit.getImportData(uploadFile.getFile(),Category.class);
        renderJson(list);
    }
}
