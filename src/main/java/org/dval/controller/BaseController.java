package org.dval.controller;

import com.jfinal.core.Controller;
import org.dval.annotation.Inject;
import org.dval.service.CategoryService;

/**
 * @author duai
 * @version V1.0
 * @Title: jfianlexample
 * @Package org.dval.controller
 * @Description: TODO
 * @date 2017-12-16 18:22
 */
public class BaseController extends Controller{
    @Inject
    CategoryService categoryService;
}
