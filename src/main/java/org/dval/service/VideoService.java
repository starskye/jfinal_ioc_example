package org.dval.service;

import org.dval.annotation.Inject;
import org.dval.annotation.Service;
import org.dval.model.Video;

/**
 * @author duai
 * @version V1.0
 * @Title: jfianlexample
 * @Package org.dval.service
 * @Description: TODO
 * @date 2017-12-12 15:30
 */
@Service
public class VideoService extends BaseService<Video> {
    @Inject
    CategoryService categoryService;
    public void print(){
        System.out.println("VideoService");
        System.out.println(categoryService.findById("1"));;
    }
}
