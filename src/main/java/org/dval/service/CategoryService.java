package org.dval.service;

import org.dval.annotation.Service;
import org.dval.model.Category;

/**
 * @author duai
 * @version V1.0
 * @Title: jfianlexample
 * @Package org.dval.service
 * @Description: TODO
 * @date 2017-12-11 17:13
 */
@Service
public class CategoryService extends BaseService<Category> {

}
