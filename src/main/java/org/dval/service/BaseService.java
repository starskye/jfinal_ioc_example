package org.dval.service;

import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Model;
import org.dval.Test;
import org.dval.annotation.Entity;
import org.dval.annotation.Inject;

import java.util.List;

/**
 * @author duai
 * @version V1.0
 * @Title: jfianlexample
 * @Package org.dval.service
 * @Description: TODO
 * @date 2017-12-11 17:11
 */
public class BaseService<M extends Model> {
    @Inject
    M dao;
    @Before(Test.class)
    public M findById(Object id){
      return (M) dao.findById(id);
    }
    public List<M> findAll(){
        return dao.find("select * from "+dao.getClass().getAnnotation(Entity.class).value());
    }
}
