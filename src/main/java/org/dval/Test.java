package org.dval;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

/**
 * @author duai
 * @version V1.0
 * @Title: jfianlexample
 * @Package org.dval
 * @Description: TODO
 * @date 2017-12-12 15:24
 */
public class Test implements Interceptor {
    @Override
    public void intercept(Invocation invocation) {
        System.out.println("测试拦截器");
        invocation.invoke();
    }
}
